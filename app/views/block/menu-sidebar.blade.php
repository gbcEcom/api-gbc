<!-- sidebar menu -->
<ul class="nav sidebar-menu">
    <li class="sidebar-label pt20">Menu</li>
    <li class="active">
        <a href="{{URL::to('/')}}">
            <span class="glyphicons glyphicons-home"></span>
            <span class="sidebar-title">Dashboard</span>
        </a>
    </li>
    <li>
        <a href="{{URL::to('section')}}">
            <span class="fa fa-folder"></span>
            <span class="sidebar-title">Sections</span>
        </a>
    </li>
    <li>
        <a href="{{URL::to('category')}}">
            <span class="fa fa-folder-o"></span>
            <span class="sidebar-title">Categories</span>
        </a>
    </li>
    <li>
        <a href="{{URL::to('post')}}">
            <span class="fa fa-newspaper-o"></span>
            <span class="sidebar-title">Posts</span>
        </a>
    </li>
    <li>
        <a href="#">
            <span class="fa fa-comments"></span>
            <span class="sidebar-title">Comments</span>
        </a>
    </li>
    <li class="sidebar-label pt15">Manage</li>
    <li>
        <a href="#">
            <span class="fa fa-home"></span>
            <span class="sidebar-title">Site</span>
        </a>
    </li>
    <li>
        <a href="#">
            <span class="fa fa-users"></span>
            <span class="sidebar-title">User</span>
        </a>
    </li>

    <li class="sidebar-label pt20">Systems</li>
    <li>
        <a class="accordion-toggle" href="#">
            <span class="glyphicons glyphicons-shopping_cart"></span>
            <span class="sidebar-title">Ecommerce</span>
            <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li>
                <a href="ecommerce_dashboard.html">
                    <span class="glyphicons glyphicons-shopping_cart"></span> Dashboard <span class="label label-xs bg-primary">New</span></a>
            </li>
            <li>
                <a href="ecommerce_products.html">
                    <span class="glyphicons glyphicons-tags"></span> Products </a>
            </li>
            <li>
                <a href="ecommerce_orders.html">
                    <span class="glyphicons glyphicons-coins"></span> Orders </a>
            </li>
            <li>
                <a href="ecommerce_customers.html">
                    <span class="glyphicons glyphicons-user_add"></span> Customers </a>
            </li>
            <li>
                <a href="ecommerce_settings.html">
                    <span class="glyphicons glyphicons-keys"></span> Store Settings </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="email_templates.html">
            <span class="fa fa-envelope-o"></span>
            <span class="sidebar-title">Email Templates</span>
                            <span class="sidebar-title-tray">
                                <span class="label label-xs bg-primary">New</span>
                            </span>
        </a>
    </li>

    <!-- sidebar resources -->
    <li class="sidebar-label pt20">Elements</li>
    <li>
        <a class="accordion-toggle" href="#">
            <span class="glyphicons glyphicons-stopwatch"></span>
            <span class="sidebar-title">Plugins</span>
            <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li>
                <a class="accordion-toggle" href="#">
                    <span class="glyphicons glyphicons-globe"></span> Maps
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="maps_advanced.html">Admin Maps</a>
                    </li>
                    <li>
                        <a href="maps_basic.html">Basic Maps</a>
                    </li>
                    <li>
                        <a href="maps_vector.html">Vector Maps</a>
                    </li>
                    <li>
                        <a href="maps_full.html">Full Map</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="accordion-toggle" href="#">
                    <span class="glyphicons glyphicons-charts"></span> Charts
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="charts_highcharts.html">Highcharts</a>
                    </li>
                    <li>
                        <a href="charts_d3.html">D3 Charts</a>
                    </li>
                    <li>
                        <a href="charts_flot.html">Flot Charts</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="accordion-toggle" href="#">
                    <span class="glyphicons glyphicons-table"></span> Tables
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="tables_basic.html"> Basic Tables</a>
                    </li>
                    <li>
                        <a href="tables_datatables.html"> DataTables </a>
                    </li>
                    <li>
                        <a href="tables_datatables-editor.html"> Editable Tables </a>
                    </li>
                    <li>
                        <a href="tables_pricing.html"> Pricing Tables </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>