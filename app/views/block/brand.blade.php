<div class="navbar-branding bg-success">
    <a class="navbar-brand" href="#"><i class="fa fa-leaf"></i> Leaf.id</a>
    <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
    <ul class="nav navbar-nav pull-right hidden">
        <li>
            <a href="#" class="sidebar-menu-toggle">
                <span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
            </a>
        </li>
    </ul>
</div>