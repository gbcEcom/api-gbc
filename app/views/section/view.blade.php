@extends('layout.default')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">Tambah Section</span>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{URL::to('section')}}">
                        <div class="form-group">
                            <label for="name">Title</label>
                            <input class="form-control" type="text" name="name" placeholder="Nama section" required="required" />
                        </div>
                        <div class="pt5">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="panel" id="p2">
                <div class="panel-heading">
                    <span class="panel-title">Sections</span>
                </div>
                <div class="panel-body">
                    <div id="tree1">
                        <ul id="treeData" style="display: none;">
                            @foreach($sections as $section)
                                <li id="{{$section->id}}" class="folder">{{$section->name}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection