<!-- Font CSS (Via CDN) -->
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/skin/default_skin/css/theme.css')}}">

<!-- Admin Panels CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin-tools/admin-plugins/admin-panels/adminpanels.css')}}">

<!-- Admin Forms CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin-tools/admin-forms/css/admin-forms.css')}}">

<!-- Favicon -->
<link rel="shortcut icon" href="{{asset('assets/img/favicon.ico')}}">

<!-- Fancytree CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('vendor/plugins/fancytree/skin-win8/ui.fancytree.min.css')}}">