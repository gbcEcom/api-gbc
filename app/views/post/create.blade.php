@extends('layout.default')

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title"><i class="fa fa-pencil"></i> Create Post</span>
                </div>
                <div class="panel-body border">
                    <form method="post" action="{{URL::to('post')}}" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Title</label>
                            <input class="form-control" type="text" name="title" placeholder="Tulis judul posting" required="required" />
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="SectionId">Section</label>
                                <select id="SectionId" class="form-control select" name="section_id">
                                    <option>Choose Section</option>
                                    @if(count($sections))
                                        @foreach($sections as $id => $section)
                                            <option value="{{$id}}">{{$section}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="CategoryId">Category</label>
                                <select id="CategoryId" class="form-control select" name="category_id">
                                    <option>Choose Category</option>
                                    @if(count($categories))
                                        @foreach($categories as $id => $category)
                                            <option value="{{$id}}">{{$category}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group admin-form">
                            <div class="section mbn">
                                <label class="field">
                                    <textarea class="gui-textarea" name="content" style="height: 350px !important;" required="required"></textarea>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Cover</label>
                            <input type="file" class="form-control" name="cover" />
                        </div>
                        <div class="form-group mbn">
                            <button class="btn btn-primary" type="submit">Publish</button>
                            <button class="btn btn-warning" type="button">Save as Draft</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection