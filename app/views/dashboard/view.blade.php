@extends('layout.default')

@section('content')
    <!-- Dashboard Tiles -->
    <div class="row mb10">
        <div class="col-md-3">
            <div class="panel panel-title text-primary br-b bw5 br-primary-light of-h mb10">
                <div class="pn pl20 p5">
                    <div class="icon-bg"> <i class="fa fa-comments-o"></i> </div>
                    <h2 class="mt15 lh15"> <b>{{$overview['comment']}}</b> </h2>
                    <h5 class="text-muted">Comments</h5>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-title text-info br-b bw5 br-info-light of-h mb10">
                <div class="pn pl20 p5">
                    <div class="icon-bg"> <i class="fa fa-newspaper-o"></i> </div>
                    <h2 class="mt15 lh15"> <b>{{$overview['post']}}</b> </h2>
                    <h5 class="text-muted">Posts</h5>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-title text-success br-b bw5 br-success-light of-h mb10">
                <div class="pn pl20 p5">
                    <div class="icon-bg"> <i class="fa fa-list-alt"></i> </div>
                    <h2 class="mt15 lh15"> <b>{{$overview['category']}}</b> </h2>
                    <h5 class="text-muted">Categories</h5>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-title text-warning br-b bw5 br-warning-light of-h mb10">
                <div class="pn pl20 p5">
                    <div class="icon-bg"> <i class="fa fa-envelope"></i> </div>
                    <h2 class="mt15 lh15"> <b>714</b> </h2>
                    <h5 class="text-muted">Mailbox</h5>
                </div>
            </div>
        </div>
    </div>

    <!-- Admin-panels -->
    <div class="admin-panels fade-onload sb-l-o-full">

        <!-- full width widgets -->
        <div class="row">

            <!-- Three panes -->
            <div class="col-md-12 admin-grid">
                <div class="panel sort-disable" id="p0">
                    <div class="panel-heading">
                        <span class="panel-title">Data Highlight</span>
                    </div>
                    <div class="panel-body border mnw700 of-a">
                        <div class="row">

                            <!-- Chart Column -->
                            <div class="col-md-4 pln br-r mvn15">
                                <h5 class="ml5 mt20 ph10 pb5 br-b fw700">Recent Posts</h5>
                                <ul>
                                    @if($recent_posts->count())
                                        @foreach($recent_posts as $post)
                                            <li class="pb5"><a href="#">{{str_limit($post->title,35)}} <b>({{$post->hits}})</b></a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>

                            <!-- Multi Text Column -->
                            <div class="col-md-4 pln br-r mvn15">
                                <h5 class="ml5 mt20 ph10 pb5 br-b fw700">Popular Posts</h5>
                                <ul>
                                    @if($popular_posts->count())
                                        @foreach($popular_posts as $post)
                                            <li class="pb5"><a href="#">{{str_limit($post->title,35)}} <b>({{$post->hits}})</b></a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>

                            <!-- Flag/Icon Column -->
                            <div class="col-md-4">
                                <h5 class="mt5 ph10 pb5 br-b fw700">Category Interest <small class="pull-right fw700 text-primary">More</small> </h5>
                                <table class="table mbn">
                                    <thead>
                                    <tr class="hidden">
                                        <th>#</th>
                                        <th>Categories</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($categories->count())
                                        @foreach($categories as $category)
                                            <tr>
                                                <td class="va-m fw600 text-muted">{{$category->name}}</td>
                                                <td class="fs15 fw700 text-right">{{$category->interest}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: .col-md-12.admin-grid -->
        </div>
        <!-- end: .row -->
    </div>
@endsection