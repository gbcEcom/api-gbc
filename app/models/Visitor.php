<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 11/17/2014
 * Time: 8:55 PM
 */

class Visitor extends Eloquent {
    //protected table
    protected $table = 'visitors';

    //Post
    public function User(){
        return $this->belongsTo('User','user_id');
    }

    //Post
    public function Activity(){
        return $this->hasMany('Activity','visitor_id');
    }
} 