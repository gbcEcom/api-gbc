<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 11/17/2014
 * Time: 8:55 PM
 */

class Category extends Eloquent {
    //protected table
    protected $table = 'categories';

    //Post
    public function Post(){
        return $this->hasMany('Post','category_id');
    }

    //Section
    public function Section(){
        return $this->belongsTo('Section','section_id');
    }

    //Parent
    public function Parent(){
        return $this->belongsTo('Category','parent_id');
    }
} 