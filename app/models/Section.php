<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 11/17/2014
 * Time: 8:55 PM
 */

class Section extends Eloquent {
    //protected table
    protected $table = 'sections';

    //Post
    public function Post(){
        return $this->hasMany('Post','section_id');
    }

    //Post
    public function Category(){
        return $this->hasMany('Category','section_id');
    }
} 