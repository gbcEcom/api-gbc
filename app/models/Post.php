<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 11/17/2014
 * Time: 8:55 PM
 */

class Post extends Eloquent {
    //protected table
    protected $table = 'posts';

    //User Profile
    public function User(){
        return $this->belongsTo('User','user_id');
    }

    //Category
    public function Category(){
        return $this->belongsTo('Category','category_id');
    }

    //Section
    public function Section(){
        return $this->belongsTo('Section','section_id');
    }

    //Comments
    public function Comments(){
        return $this->hasMany('Comment','id','post_id');
    }

    //Comments
    public function Likes(){
        return $this->hasMany('Like','id','post_id');
    }
} 