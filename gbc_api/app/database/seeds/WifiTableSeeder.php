<?php

class WifiTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

    public function run()
    {
        $data = [
            'username'          => 'shaladin',
            'limit_quota'       => 86400,
            'limit_bandwidth'   => 0,
            'usage'             => 0,
            'volume'            => 0,
            'status'            => 1
        ];

        $password = hash('sha256', hash('sha256', $data['username']) . hash('sha256', 'grober2015'));
        $data['password'] = $password;
        // store data to table
        Wifi::create($data);
    }

}
