<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWifiId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wifies', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('username');
            $table->string('password',100);
            $table->integer('limit_quota');
            $table->integer('limit_bandwidth');
            $table->integer('usage');
            $table->integer('volume');
            $table->tinyInteger('status');
            $table->integer('package_id');
            $table->string('trxid');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wifies');
	}

}
