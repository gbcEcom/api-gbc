<?php

class AccountController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$PageTitle = 'Manage Account';
        //Get Wifi.ID accounts
        $accounts  = Wifi::orderBy('id','desc')->paginate(10);

        return View::make('account.manage', compact('PageTitle', 'accounts'));
	}

    /**
     *
     * Process Create User
     */
    public function postIndex()
    {
        $data = [
            'username'          => Input::get('username'),
            'limit_quota'       => 86400,
            'limit_bandwidth'   => 0,
            'usage'             => 0,
            'volume'            => 0,
            'status'            => 0
        ];

        $password = hash('sha256', hash('sha256', $data['username']) . hash('sha256', Input::get('password')));
        $data['password'] = $password;

        $create = Wifi::create($data);
        if($create)
        {
            return Redirect::to('account')
                ->with('message', 'Wifi.id account success created, id: '.$create->id);
        }
        return Redirect::to('account');
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
