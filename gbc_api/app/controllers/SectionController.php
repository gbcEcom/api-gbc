<?php

class SectionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sections = Section::all();

        return View::make('section.view',compact('sections'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $section = Section::find($id);

        $post = Post::with('Category')
            ->with('Section')
            ->with('User')
            ->where('section_id',$id)
            ->orderBy('id','desc')
            ->paginate(6);

        if(!is_null($post)){
            $_data = array(
                'error' => false,
                'section' => $section->toArray(),
                'posts'  => $post->toArray()
            );
        } else {
            $_data = array(
                'error' => true,
                'section' => $section->toArray(),
                'message' => 'Sorry data not found or removed !'
            );
        }

        return Response::json($_data, 200);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
