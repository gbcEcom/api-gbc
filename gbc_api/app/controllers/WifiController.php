<?php

class WifiController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function postAuth()
	{
		// Get Variable from Requests
        $password = Input::get('password');
        $username = Input::get('username');
        // Salt the password
        //$hash_pass= hash('sha256', hash('sha256', $username) . hash('sha256', $password));
        $hash_pass= md5($password);

        // response data
        $response = array();

        if (Input::has('username') && Input::has('password'))
        {
            // Check username and account status
            $auth = Wifi::where('username', $username)->first();

            if ($auth->count())
            {
                if ($auth->status == 1 && $auth->flag == 1) //Account Activated
                {
                    // compare and validate password
                    if ($auth->password == $hash_pass) // password passed
                    {
                        $response = [$auth->password,'OK',$auth->limit_quota,$auth->limit_bandwidth];
                    }
                    else // wrong password
                    {
                        $response = [$hash_pass,'Not OK','Wrong Password'];
                    }
                }
                else //Account suspended
                {
                    $response = [$hash_pass,'Not OK','Account Suspended'];
                }
            }
        }
        else
        {
            $response = [$hash_pass,'Not OK','Invalid Account'];
        }

        return $data = implode(',', $response);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postUsage()
	{
		// Collect variable requests
        $trxid      = Input::get('trxid');
        $username   = Input::get('username');
        $usage      = Input::get('usage');
        $volume     = Input::get('volume');

        // Response data
        $response   = array();

        // Check username account
        $wifi = Wifi::where('username', $username)->first();

        if ($wifi->count()) //account founded
        {
            //update account billing usage
            $wifi->trxid    = $trxid;
            $wifi->usage    = $usage;
            $wifi->volume   = $volume;
            $wifi->save();

            //initial response
            $response = [$trxid, 'OK'];
        }
        else //account username not found | invalid
        {
            $response = [$trxid, 'Not OK'];
        }

        return $data = implode(',', $response);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
