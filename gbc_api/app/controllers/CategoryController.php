<?php

class CategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Request::ajax())
        {
            $categories = Category::where('section_id', Input::get('section_id'))->get();

            return Response::json(array(
                'error' => false,
                'categories' => $categories->toArray()
            ),200);
        }

        $sections   = Section::lists('name','id');
        $categories = Category::paginate(10);

        return View::make('category.view', compact('categories','sections'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

        $category = new Category();
        $category->name = Input::get('name');
        $category->slug = strtolower(Str::random(16));
        $category->section_id = Input::get('section_id');
        $category->seo_name = Input::get('seo_name');
        $category->description = Input::get('description');
        $category->status = 1;

        if ($category->save()) {
            return Redirect::to('category');
        }

        return Redirect::to('category')
            ->with('message', 'Category failed to save');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
