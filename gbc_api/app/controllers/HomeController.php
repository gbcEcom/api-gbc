<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
        $overview = array(
            'comment' => Comment::all()->count(),
            'post'    => Post::all()->count(),
            'category'=> Category::all()->count()
        );

        $popular_posts= Post::orderBy('hits','desc')->take(10)->get();
        $recent_posts = Post::orderBy('id','desc')->take(10)->get();
        $categories   = Category::select(DB::raw("categories.id,categories.name,sum(posts.hits) as interest"))
                        ->join('posts','categories.id','=','posts.category_id','left')
                        ->groupBy('categories.id')
                        ->orderBy('interest','desc')
                        ->take(6)
                        ->get();

        //$comments     = Comment::orderBy('id','desc')->take(10)->get();

        return View::make('dashboard.view', compact('popular_posts','recent_posts','comments','overview','categories'));
	}

}
