<?php

class ApiController extends \BaseController {

    protected $username = 'admin@grosirbersama.com';
    protected $prodKey  = '7Rkt5Py660EZxMvTI60dd66N2w5uz83k';
    protected $devKey   = '0x4Ps27C4KFpWp1A94372ddru3220dwi';
    // protected $API_URI  = 'http://www.grosirbersama.co.id/api/';
    protected $API_URI  = 'http://localhost/gbc/api/';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        $client = new GuzzleHttp\Client();
        $res    = $client->get($this->API_URI.'products/41550', [
                'auth' =>  [ $this->username, $this->prodKey ]
        ]);
        

        $data= json_decode($res->getBody());

        //echo $data;

        //return $data;
        //echo $res->getStatusCode(); // 200

        return Response::json(array(
            'status'    => 'success',
            'response'  => $data
        ),200); // { "type": "User", ....

	}

    /**
     * AUTH USER
     */
    public function getAuth(){
        //Get Request Variable
        $email   = Input::get('email');
        $password= Input::get('password');

        $authUser= null;
        //Check User Available
        $checkUser    = User::where('email', $email)->first();
        //Validating User
        if($checkUser){
            //Hash password with standard encryption
            $md5_password = md5($password);
            $md5_salt     = md5($checkUser->salt);

            //Final Hash Password
            $hash_password= md5($md5_password.$md5_salt);
            //Prepare Client Request
            $client = new GuzzleHttp\Client();

            if ($checkUser->password == $hash_password){
                $request= $client->request('GET', $this->API_URI.'users/'.$checkUser->user_id,[
                    'auth'  => [$this->username, $this->prodKey]
                ]);

                $responses = json_decode($request->getBody());

                $_data = array(
                    'isAuth'    => true,
                    'key'       => $checkUser->api_key,
                    'data'      => $responses
                );
            } else {
                $_data = array(
                    'isAuth'    => false,
                    'key'       => null,
                    'data'      => 'Password yang anda masukan salah !'
                );
            }
        } else {
            $_data = array(
                'isAuth'    => false,
                'key'       => null,
                'data'      => 'User tidak di temukan !'
            );
        }

        return Response::json($_data, 200);
    }

    /*
     * SUMMARY
     */
    public function getSummary(){
        //Get Input Email
        $email        = Input::get('email');
        $token        = '7Rkt5Py660EZxMvTI60dd66N2w5uz83k';

        //Check User
        $checkUser    = User::where('email', $email)->first();

        if ($checkUser){
            //Prepare API Client Request
            $client = new GuzzleHttp\Client();
            $token  = $checkUser->api_key;

            //Get Cusotmers Total
            $customer= $client->request('GET', $this->API_URI.'users?user_type=C',[
                'auth'  => [$email, $token]
            ]);

            //Get Product Total
            $product= $client->request('GET', $this->API_URI.'products', [
                'auth'  => [$email, $token]
            ]);

            //Get Order Total
            $order= $client->request('GET', $this->API_URI.'orders?status=O', [
                'auth'  => [$email, $token]
            ]);

            //Get Sales Total
            $sales= $client->request('GET', $this->API_URI.'orders?status=C', [
                'auth'  => [$email, $token]
            ]);

            $getSale = json_decode($sales->getBody());

            $total_sale = 0;
            foreach($getSale->orders as $sale){
                $total_sale  = $total_sale + $sale->total;
            }

            $total_product = json_decode($product->getBody());
            $total_customer= json_decode($customer->getBody());
            $total_order   = json_decode($order->getBody());

            $_data = array(
                'total_sale'    => $total_sale,
                'total_order'   => $total_order->params->total_items,
                'total_customer'=> $total_customer->params->total_items,
                'total_product' => $total_product->params->total_items
            );

        } else {
            $_data = 'Your are not have permission to access this area !';
        }

        return Response::json($_data, 200);
    }

	/**
	 * PRODUCTS
	 *
	 * @return Response
	 */
	public function postCreate()
	{
        if (Input::has('image_path') && Input::get('image_path') != ''){
            $image_path = 'http://api.grosirbersama.co.id/'.Input::get('image_path');
        } else {
            $image_path = 'http://demo.grosirbersama.co.id/images/promo/28/banner_lama_pemenang_xiaomi.jpg';
        }

        $client = new GuzzleHttp\Client();
        $_data = array(
            'product'       => Input::get('product'),
            'category_ids'  => Input::get('category_id'),
            'main_category' => Input::get('category_id'),
            'company_id'    => Input::get('company_id'),
            'price'         => Input::get('price'),
            'amount'        => Input::get('stock'),
            'min_qty'       => Input::get('min_qty'),
            'qty_step'      => 1,
            'weight'        => Input::get('weight'),
	        'full_description' => Input::get('description'),
            'main_pair'     => array(
                'detailed'  => array(
                    'image_path'    => $image_path
                )
            )
        );
        /*
        $_data = array(
            'product'       => 'XIOMI',
            'category_ids'  => 165,
            'main_category' => 165,
            'company_id'    => 11,
            'price'         => 1500000,
            'amount'        => 5,
            'min_qty'       => 1,
            'weight'        => 1
        );
        */
        try{
            $r = $client->request('POST', $this->API_URI.'products', [
                'auth' =>   [$this->username, $this->prodKey],
                'json' =>   $_data
            ]);

            $response  = json_decode($r->getBody());
            $product_id= $response->product_id;

            return Response::json(array(
                'error'     =>  false,
                'product_id'=>  $product_id,
                'message'   =>  'Produk berhasil di simpan, Produk ID: '.$product_id
            ),200);
        } catch (Exception $e) {
            return Response::json(array(
                'error'     => true,
                'message'   => $e->getMessage()
            ),200);
        }
	}

    public function postUpdateProduct($id)
    {
        $client = new GuzzleHttp\Client();
        $_data = array(
            'product'       => Input::get('product'),
            'price'         => Input::get('price'),
            'amount'        => Input::get('stock'),
            'min_qty'       => Input::get('min_qty'),
            'qty_step'      => 2,
            'weight'        => Input::get('weight')
            //'full_description' => Input::get('description')
        );

        try{
            $r = $client->request('PUT', $this->API_URI.'products'.'/'.$id, [
                'auth' =>   [$this->username, $this->prodKey],
                'json' =>   $_data
            ]);

            $response  = json_decode($r->getBody());
            $product_id= $response->product_id;

            return Response::json(array(
                'error'     =>  false,
                'product_id'=>  $product_id,
                'message'   =>  'Produk berhasil di perbaharui, Produk ID: '.$product_id
            ),200);
        } catch (Exception $e) {
            return Response::json(array(
                'error'     => true,
                'message'   => $e->getMessage()
            ),200);
        }
    }
    
    public function getPayments($id = null){
        if(Input::has('company_id')){
            $company_id = Input::get('company_id');
            $params = '?company_id='.$company_id;

            if(Input::has('page')){
                $params .= '&page='.Input::get('page');
            }

        } elseif ($id != null){
            $params = '/'.$id;
        }

        else {
            $params = '/';
        }
        
        $client  = new GuzzleHttp\Client();
        $request = $client->request('GET', $this->API_URI.'payments'.$params,[
            'auth'  =>  [$this->username, $this->prodKey]
        ]);

        $payments = json_decode($request->getBody());
        
        $pay = [];
        
        if($id==null){
            foreach($payments->payments as $c){
                if($c->status=='A'){
                    $pay['payments'][] = $c;
                }
            }
            $pay['params'] = $payments->params;
        }
        else{
            $pay = [
                
            ];
        }
        
        return Response::json(array(
            'error'     => false,
            'response'  => $pay
        ),200);
    }

    public function getProducts($id = null){
        $category = '';
        
        if(Input::has('sort_by')){
            $sort_by = Input::get('sort_by');
            $sort_order = Input::has('sort_order') ? Input::get('sort_order') : 'desc' ;
            $params = '?sort_by='.$sort_by.'&sort_order='.$sort_order;
        }
        elseif(Input::has('location')){
            $location = Input::get('location');
            $params = '?location='.$location;
        }
        elseif(Input::has('company_id')){
            $company_id = Input::get('company_id');
            $params = '?company_id='.$company_id.'&sort_by=timestamp&sort_order=desc';

            if(Input::has('page')){
                $params .= '&page='.Input::get('page');
            }

        } 
        elseif(Input::has('category_id')){
            $category = 'categories/'.Input::get('category_id').'/';
            $params = '';
        }
        elseif ($id != null){
            $params = '/'.$id;
        }

        else {
            $params = '/';
        }
        
        $client  = new GuzzleHttp\Client();
        $request = $client->request('GET', $this->API_URI.$category.'products'.$params,[
            'auth'  =>  [$this->username, $this->prodKey]
        ]);

        $products = json_decode($request->getBody());
        // print_r("<pre>");
        // print_r($products);
        // die;
        
        
        // print_r("<pre>");
        // foreach($products->products as $p){
            // print_r(date("Y-m-d H:i:s", $p->timestamp));
            // print_r(' - ');
            // print_r(date("Y-m-d H:i:s", $p->updated_timestamp));
            // print_r(' - ');
            // print_r($p->product);
            
            
            // print_r('<br>');
        // }
        // die;
        

        $pro = [];
        
        if($id==null){            
            foreach($products->products as $p){
                $pro['products'][] = [
                    'product_id' => $p->product_id,
                    'product' => $p->product,
                    'price' => $p->price,
                    'company_id' => $p->company_id,
                    'status' => $p->status,
                    'amount' => $p->amount,
                    'list_price' => $p->list_price,
                    'main_pair' => isset($p->main_pair) ? $p->main_pair : '',
                    'min_qty' => $p->min_qty,
                    'weight' => $p->weight,
                    'popularity' => $p->popularity,
                    // 'short_description' => $p->short_description,
                    // 'full_description' => $p->full_description,
                ];
            }
            $pro['params'] = $products->params;
        }
        else{
            $pro = [
                'product_id' => $products->product_id,
                'product' => $products->product,
                'price' => $products->price,
                'company_id' => $products->company_id,
                'status' => $products->status,
                'amount' => $products->amount,
                'list_price' => $products->list_price,
                'main_pair' => isset($products->main_pair) ? $products->main_pair : '',
                'min_qty' => $products->min_qty,
                'weight' => $products->weight,
                'short_description' => $products->short_description,
                'full_description' => $products->full_description,
                'popularity' => $products->popularity,
            ];
        }
        
        return Response::json(array(
            'error'     => false,
            'response'  => $pro
        ),200);
    }
    
    /* ---- api/products?category_id=${id}
    public function getProductcategories($id1 = null, $id2 = null){
        if(Input::has('company_id')){
            $company_id = Input::get('company_id');
            $params = '?company_id='.$company_id.'&sort_by=timestamp&sort_order=desc';

            if(Input::has('page')){
                $params .= '&page='.Input::get('page');
            }

        } elseif ($id2 != null){
            $params = '/'.$id2;
        }

        else {
            $params = '/';
        }

        $client  = new GuzzleHttp\Client();
        $request = $client->request('GET', $this->API_URI.'categories/'.$id1.'/products'.$params,[
            'auth'  =>  [$this->username, $this->prodKey]
        ]);

        $products = json_decode($request->getBody());
        
        $pro = [];
        
        if($id2==null){            
            foreach($products->products as $p){
                $pro['products'][] = [
                    'product_id' => $p->product_id,
                    'product' => $p->product,
                    'price' => $p->price,
                    'company_id' => $p->company_id,
                    'status' => $p->status,
                    'amount' => $p->amount,
                    'list_price' => $p->list_price,
                    'main_pair' => isset($p->main_pair) ? $p->main_pair : '',
                    'min_qty' => $p->min_qty,
                    'weight' => $p->weight,
                    // 'short_description' => $p->short_description,
                    // 'full_description' => $p->full_description,
                    // 'popularity' => $p->popularity,
                ];
            }
            $pro['params'] = $products->params;
        }
        else{
            $pro = [
                'product_id' => $products->product_id,
                'product' => $products->product,
                'price' => $products->price,
                'company_id' => $products->company_id,
                'status' => $products->status,
                'amount' => $products->amount,
                'list_price' => $products->list_price,
                'main_pair' => isset($products->main_pair) ? $products->main_pair : '',
                'min_qty' => $products->min_qty,
                'weight' => $products->weight,
                'short_description' => $products->short_description,
                'full_description' => $products->full_description,
                'popularity' => $products->popularity,
            ];
        }
        
        return Response::json(array(
            'error'     => false,
            'response'  => $pro
        ),200);
    }*/
    
    public function getCategories($id = null){
        if(Input::has('company_id')){
            $company_id = Input::get('company_id');
            $params = '?company_id='.$company_id;

            if(Input::has('page')){
                $params .= '&page='.Input::get('page');
            }

        } elseif ($id != null){
            $params = '/'.$id;
        }

        else {
            $params = '/';
        }
        
        $client  = new GuzzleHttp\Client();
        $request = $client->request('GET', $this->API_URI.'categories'.$params,[
            'auth'  =>  [$this->username, $this->prodKey]
        ]);

        $categories = json_decode($request->getBody());
        
        $cat = [];
        
        if($id==null){
            foreach($categories->categories as $c){
                $cat['categories'][] = [
                    'category_id' => $c->category_id,
                    'category' => $c->category,
                    'status' => $c->status,
                    'main_pair' => isset($c->main_pair) ? $c->main_pair : '',
                    'parent_id' => $c->parent_id,
                    'position' => $c->position,
                    // 'company_id' => $c->company_id,
                ];
            }
            $cat['params'] = $categories->params;
        }
        else{
            $cat = [
                'category_id' => $categories->category_id,
                'category' => $categories->category,
                'status' => $categories->status,
                'main_pair' => isset($categories->main_pair) ? $categories->main_pair : '',
                'parent_id' => $categories->parent_id,
                'company_id' => $categories->company_id,
                'position' => $categories->position,
            ];
        }
        
        return Response::json(array(
            'error'     => false,
            'response'  => $cat
        ),200);
    }
    
    public function getUsers($id = null){
        if(Input::has('company_id')){
            $company_id = Input::get('company_id');
            $params = '?company_id='.$company_id;

            if(Input::has('page')){
                $params .= '&page='.Input::get('page');
            }

        } elseif ($id != null){
            $params = '/'.$id;
        }

        else {
            $params = '/';
        }
        
        $client  = new GuzzleHttp\Client();
        $request = $client->request('GET', $this->API_URI.'users'.$params,[
            'auth'  =>  [$this->username, $this->prodKey]
        ]);

        $users = json_decode($request->getBody());
        
        $us = [];
        
        if($id==null){
            foreach($users->users as $u){
                $us['users'][] = [
                    'user_id' => $u->user_id,
                    'email' => $u->email,
                    'user_type' => $u->user_type,
                    'company_id' => $u->company_id,
                    'status' => $u->status,
                    'firstname' => $u->firstname,
                    'lastname' => $u->lastname,
                    'company' => $u->company,
                    'company_name' => $u->company_name,
                    // 'password' => $u->password,
                ];
            }
            $us['params'] = $users->params;
        }
        else{
            $us = [
                'user_id' => $users->user_id,
                'email' => $users->email,
                'user_type' => $users->user_type,
                'company_id' => $users->company_id,
                'status' => $users->status,
                'firstname' => $users->firstname,
                'lastname' => $users->lastname,
                'company' => $users->company,
                'company_name' => $users->company_name,
                // 'password' => $users->password,
            ];
        }
        
        return Response::json(array(
            'error'     => false,
            'response'  => $us
        ),200);
    }

    /**
     * ORDERS
     */
    public function getOrders($id = null){

        if(Input::has('user_id')){
            $params = '?user_id='.Input::get('user_id');
        }
        elseif(Input::has('company_id')){
            $company_id = Input::get('company_id');
            $params = '?company_id='.$company_id;

            if(Input::has('page')){
                $params .= '&page='.Input::get('page');
            }

        } elseif ($id != null){
            $params = '/'.$id;
        }

        else {
            $params = '/';
        }        

        $client = new GuzzleHttp\Client();
        $request= $client->request('GET', $this->API_URI.'orders'.$params, [
            'auth'  =>  [$this->username, $this->prodKey]
        ]);

        $orders = json_decode($request->getBody());
        
        $ord = [];
        if($id==null){
            foreach($orders->orders as $o){
                $ord['orders'][] = [
                    'order_id' => $o->order_id,
                    'is_parent_order' => $o->is_parent_order,
                    'parent_order_id' => $o->parent_order_id,
                    'status' => $o->status,
                    'company_id' => $o->company_id,
                    'user_id' => $o->user_id,
                    'firstname' => $o->firstname,
                    'lastname' => $o->lastname,
                    'email' => $o->email,
                    'phone' => $o->phone,
                    'total' => $o->total,
                    'invoice_id' => $o->invoice_id,
                    // 'discount' => $o->discount,
                    // 'subtotal' => $o->subtotal,
                    // 'payment_id' => $o->payment_id,
                    // 'payment_method' => $o->payment_method,
                    // 'payment_surcharge' => $o->payment_surcharge,
                ];
            }
            $ord['params'] = $orders->params;
        }
        else{
            $ord = [
                'order_id' => $orders->order_id,
                'is_parent_order' => $orders->is_parent_order,
                'parent_order_id' => $orders->parent_order_id,
                'status' => $orders->status,
                'company_id' => $orders->company_id,
                'user_id' => $orders->user_id,
                'firstname' => $orders->firstname,
                'lastname' => $orders->lastname,
                'email' => $orders->email,
                'phone' => $orders->phone,
                'total' => $orders->total,
                // 'invoice_id' => $orders->invoice_id,
                'discount' => $orders->discount,
                'subtotal' => $orders->subtotal,
                'payment_id' => $orders->payment_id,
                'payment_method' => $orders->payment_method,
                'payment_surcharge' => $orders->payment_surcharge,
                // 'shipping-ids' => $orders->shipping_ids,
                'b_firstname' => $orders->b_firstname,
                'b_lastname' => $orders->b_lastname,
                'b_address' => $orders->b_address,
                'b_city' => $orders->b_city,
                'b_country' => $orders->b_country,
                'b_state' => $orders->b_state,
                'b_zipcode' => $orders->b_zipcode,
                'b_phone' => $orders->b_phone,
                'b_kabupaten' => $orders->b_kabupaten,
                's_firstname' => $orders->s_firstname,
                's_lastname' => $orders->s_lastname,
                's_address' => $orders->s_address,
                's_city' => $orders->s_city,
                's_country' => $orders->s_country,
                's_state' => $orders->s_state,
                's_zipcode' => $orders->s_zipcode,
                's_phone' => $orders->s_phone,
                's_kabupaten' => $orders->s_kabupaten,
            ];
        }
        
        return Response::json(array(
            'error'     => false,
            'response'  => $ord
        ),200);
    }

    public function postOrders(){
        $_data = [
            'user_id' => Input::get('user_id'),
            'shipping_id' => Input::get('shipping_id'),
            'payment_id' => Input::get('payment_id'),
            'products' => Input::get('products'),
            'notes' => Input::get('notes'),
        ];
        
        /*$_data = [
            "user_id" => "13618",
            "shipping_id" => "6",
            "payment_id" => "12",
            "products" => [
                "40829" => [
                    "product_id" => "40829",
                    "amount" => "2",
                ]
            ],
            'notes' => 'cek123'
        ];*/
        
        $client = new GuzzleHttp\Client();
        $request = $client->request('POST', $this->API_URI.'orders', [
            'auth'  => [$this->username, $this->prodKey],
            'json'  => $_data
        ]);
        
        $response = json_decode($request->getBody());

        return Response::json($response, 200);
    }
    
    public function postOrdersStatus(){

        $status     = Input::get('status');
        $order_id   = Input::get('order_id');

        $client = new GuzzleHttp\Client();
        $request= $client->request('PUT', $this->API_URI.'orders/'.$order_id, [
            'auth'  => [$this->username, $this->prodKey],
            'json'  => ['status' => $status]
        ]);

        $response = json_decode($request->getBody());

        return Response::json($response, 200);
    }

    public function postUsers(){
        $salt     = md5('Dj-bSpRV4.');
        $password = md5(Input::get('password'));

        // Create new store
        $store= $this->newStore(Input::get('email'), Input::get('store'), Input::get('address'));

        // Create new user
        $user = new User();
        $user->user_type    = 'V';
        $user->user_login   = Input::get('email');
        $user->is_root      = 'Y';
        $user->firstname    = Input::get('firstname');
        $user->lastname     = Input::get('lastname');
        $user->company_id   = $store;
        $user->salt         = 'Dj-bSpRV4.';
        $user->email        = Input::get('email');
        $user->password     = md5($password.$salt);
        $user->company      = Input::get('store');
        $user->phone        = Input::get('phone');
        $user->timestamp    = time();
        
        if ($user->save()){
            $_data = array(
                'status'    => true,
                'user_id'   => $user->id
            );
        } else {
            $_data = array(
                'status'    => false,
                'message'   => 'Maaf gagal membuka toko dan user baru !'
            );
        }

        return Response::json($_data, 200);
    }

    private function newStore($email,$company, $address){
        $store = new Company();

        $store->email = $email;
        $store->company  = $company;
        $store->address  = $address;
        $store->timestamp= time();
        $store->save();

        return $store->id;
    }
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $cover = '';

        if (Input::hasFile('images')){
            //Path to store posts image
            $PathImage = public_path('assets/images/upload/');
            $cover     = strtolower(str_random(16)) . '.' . Input::file('images')->getClientOriginalExtension();

            //Move uploaded image
            if(Input::file('images')->move($PathImage, $cover)){
                //create large image
                Image::make($PathImage.$cover)->resize(1045,500)->save($PathImage.'lg_'.$cover);
            }

            $_data = array(
                'status' => 'success',
                'image'  => $cover
            );
        } else {
            $_data = array(
                'status' => 'failed',
                'image'  => 'not found image key'
            );
        }

        return Response::json($_data, 200);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::with('Category')
            ->with('User')
            ->where('slug',$id)->first();

        if(!is_null($post)){
            $_data = array(
                'error' => false,
                'post'  => $post->toArray()
            );
        } else {
            $_data = array(
                'error' => true,
                'message' => 'Sorry data not found or removed !'
            );
        }

        return Response::json($_data, 200);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

        $post = Post::where('slug',$id)->get();
        $sections   = Section::orderBy('name','asc')->lists('name','id');
        $categories = Category::where('section_id',$post[0]->section_id)->orderBy('name','asc')->lists('name','id');

        //echo '<pre>';
        //echo print_r($post);
        //exit;

        return View::make('post.edit', compact('post','sections','categories'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

        $Post  = Post::where('slug', Input::get('slug'))->first();

        if (Input::hasFile('cover')){
            //Path to store posts image
            $PathImage = public_path('assets/images/posts/');
            $cover     = strtolower(str_random(16)) . '.' . Input::file('cover')->getClientOriginalExtension();

            //Move uploaded image
            if(Input::file('cover')->move($PathImage, $cover)){
                //create large image
                Image::make($PathImage.$cover)->resize(1045,500)->save($PathImage.'lg_'.$cover);
                Image::make($PathImage.$cover)->resize(410,250)->save($PathImage.'md_'.$cover);
                Image::make($PathImage.$cover)->resize(100,100)->save($PathImage.'sm_'.$cover);
            }
        } else {
            $cover = $Post->image;
        }

        $Post->title = Input::get('title');
        $Post->section_id = Input::get('section_id');
        $Post->category_id= Input::get('category_id');
        $Post->image = $cover;
        $Post->content = Input::get('konten');
        $Post->user_id = 1;
        $Post->save();

        return Redirect::to('post');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
