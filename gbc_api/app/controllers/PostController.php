<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sortBy = 'id';

        if (Input::has('order_by')){
            $sortBy = Input::get('order_by');
        }

        $posts = Post::where('section_id','!=',20)
            ->with('User')
            ->with('Category')
            ->with('Section')
            ->orderBy($sortBy,'desc')
            ->paginate(11);

        return Response::json(array(
            'error' => false,
            'posts' => $posts->toArray()
        ),200);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$sections   = Section::orderBy('name','asc')->lists('name','id');
        $categories = Category::orderBy('name','asc')->lists('name','id');

        return View::make('post.create', compact('sections','categories'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$cover = '';

        if (Input::hasFile('images')){
            //Path to store posts image
            $PathImage = public_path('assets/images/upload/');
            $cover     = strtolower(str_random(16)) . '.' . Input::file('cover')->getClientOriginalExtension();

            //Move uploaded image
            if(Input::file('cover')->move($PathImage, $cover)){
                //create large image
                Image::make($PathImage.$cover)->resize(1045,500)->save($PathImage.'lg_'.$cover);
            }

            $_data = array(
                'status' => 'success',
                'image'  => $cover
            );
        } else {
            $_data = array(
                'status' => 'failed',
                'image'  => 'not found image key'
            );
        }

        return Response::json($_data, 200);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::with('Category')
            ->with('User')
            ->where('slug',$id)->first();

        if(!is_null($post)){
            $_data = array(
                'error' => false,
                'post'  => $post->toArray()
            );
        } else {
            $_data = array(
                'error' => true,
                'message' => 'Sorry data not found or removed !'
            );
        }

        return Response::json($_data, 200);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

        $post = Post::where('slug',$id)->get();
        $sections   = Section::orderBy('name','asc')->lists('name','id');
        $categories = Category::where('section_id',$post[0]->section_id)->orderBy('name','asc')->lists('name','id');

        //echo '<pre>';
        //echo print_r($post);
        //exit;

        return View::make('post.edit', compact('post','sections','categories'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

        $Post  = Post::where('slug', Input::get('slug'))->first();

        if (Input::hasFile('cover')){
            //Path to store posts image
            $PathImage = public_path('assets/images/posts/');
            $cover     = strtolower(str_random(16)) . '.' . Input::file('cover')->getClientOriginalExtension();

            //Move uploaded image
            if(Input::file('cover')->move($PathImage, $cover)){
                //create large image
                Image::make($PathImage.$cover)->resize(1045,500)->save($PathImage.'lg_'.$cover);
                Image::make($PathImage.$cover)->resize(410,250)->save($PathImage.'md_'.$cover);
                Image::make($PathImage.$cover)->resize(100,100)->save($PathImage.'sm_'.$cover);
            }
        } else {
            $cover = $Post->image;
        }

        $Post->title = Input::get('title');
        $Post->section_id = Input::get('section_id');
        $Post->category_id= Input::get('category_id');
        $Post->image = $cover;
        $Post->content = Input::get('konten');
        $Post->user_id = 1;
        $Post->save();

        return Redirect::to('post');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
