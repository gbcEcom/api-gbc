<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', function()
{
	return View::make('account.manage');
});


Route::controller('auth', 'AuthController');
Route::resource('section', 'SectionController');
Route::resource('category', 'CategoryController');
Route::resource('post', 'PostController');
Route::resource('upload', 'UploadController');
Route::controller('api', 'ApiController');
Route::controller('wifi', 'WifiController');
#Route::resource('/', 'HomeController');
Route::controller('account', 'AccountController');