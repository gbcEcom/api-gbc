<!-- Bootstrap 3.3.5 -->
{{HTML::style('assets/bootstrap/css/bootstrap.min.css')}}
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
{{HTML::style('assets/dist/css/AdminLTE.min.css')}}
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
{{HTML::style('assets/dist/css/skins/_all-skins.min.css')}}
<!-- iCheck -->
{{HTML::style('assets/plugins/iCheck/flat/blue.css')}}
<!-- Morris chart -->
{{HTML::style('assets/plugins/morris/morris.css')}}
<!-- jvectormap -->
{{HTML::style('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}
<!-- Date Picker -->
{{HTML::style('assets/plugins/datepicker/datepicker3.css')}}
<!-- Daterange picker -->
{{HTML::style('assets/plugins/daterangepicker/daterangepicker-bs3.css')}}
<!-- bootstrap wysihtml5 - text editor -->
{{HTML::style('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}