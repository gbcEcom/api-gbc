@extends('layout.admin')

@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">Create Account</h2>
            </div>
            <div class="panel-body">
                <form method="post" action="{{URL::to('account')}}">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" placeholder="type your account username" required="required" />
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="type your account username" required="required" />
                    </div>
                    <button type="submit" class="btn btn-success">Create</button>
                </form>
            </div>
        </div>

        @if(Session::has('message'))
            <div class="alert alert-warning">
                <p>{{Session::get('message')}}</p>
            </div>
        @endif
    </div>
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title">Accounts</h2>
            </div>
            <div class="panel-body">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Username</th>
                            <th>Usage</th>
                            <th>Expired</th>
                            <th>Status</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($accounts))
                        @foreach($accounts as $account)
                            <tr>
                                <td>{{$account->id}}</td>
                                <td>{{$account->username}}</td>
                                <td>{{$account->usage}}</td>
                                <td>{{$account->updated_at}}</td>
                                <td>{{($account->status) ? 'Active' : 'Pending'}}</td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <a href="#" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
                                        <a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                        <a href="#" class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">Account not found !</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                {{$accounts->links()}}
            </div>
        </div>
    </div>
</div>
@stop