@extends('layout.default')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">Tambah Kategori</span>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{URL::to('category')}}">
                        <div class="form-group">
                            <label for="name">Kategori</label>
                            <input class="form-control" type="text" name="name" placeholder="Nama kategori" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="SectionId">Section</label>
                            <select id="SectionId" class="form-control select" name="section_id">
                                <option>Choose Section</option>
                                @if(count($sections))
                                    @foreach($sections as $id => $section)
                                        <option value="{{$id}}">{{$section}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Seo Name</label>
                            <input class="form-control" type="text" name="seo_name" placeholder="Nama seo kategori" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="name">Description</label>
                            <textarea class="form-control" name="description" placeholder="Write description about this category"></textarea>
                        </div>
                        <div class="pt5">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <span class="panel-title">Categories</span>
                </div>
                <div class="panel-body border">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Category</th>
                            <th>Section</th>
                            <th>Parent</th>
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($categories->count())
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{$category->name}}</td>
                                    <td>{{$category->section->name}}</td>
                                    <td>{{($category->parent_id) ? $category->parent->name : '-'}}</td>
                                    <td>{{($category->status != 0) ? 'Active' : 'Disable'}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection