@extends('layout.default')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default mbn">
                <div class="panel-heading">
                    <span class="panel-title"><i class="fa fa-newspaper-o"></i> Posts</span>
                    <div class="panel-header-menu pull-right mr10">
                        <a href="{{URL::to('post/create')}}" class="btn btn-xs btn-primary"><i class="fa fa-plus-circle"></i> Create Post</a>
                    </div>
                </div>
                <div class="panel-body border pn">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>IDs</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Hits</th>
                            <th>Created</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($posts->count())
                            @foreach($posts as $post)
                                <tr>
                                    <td>#{{$post->id}}</td>
                                    <td>{{$post->title}}</td>
                                    <td>{{(is_null($post->category)) ? '-' : $post->category->name}}</td>
                                    <td>{{$post->hits}}</td>
                                    <td>{{date('d M Y', time($post->created_at))}}</td>
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <a href="{{URL::to('post/'.$post->slug.'/edit')}}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
                                            <a href="{{URL::to('post/'.$post->slug)}}" class="btn btn-xs btn-default disabled"><i class="fa fa-star"></i></a>
                                            <a href="{{URL::to('post/'.$post->slug)}}" class="btn btn-xs btn-default disabled"><i class="fa fa-remove"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{$posts->appends(array_except(Input::query(), Paginator::getPageName()))->links()}}
        </div>
    </div>
@endsection