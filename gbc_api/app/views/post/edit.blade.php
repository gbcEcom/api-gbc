@extends('layout.default')

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title"><i class="fa fa-pencil"></i> Edit Post</span>
                </div>
                <div class="panel-body border">
                    <form method="post" action="{{URL::to('post/'.$post[0]->slug)}}" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="put" />
                        <input type="hidden" name="slug" value="{{$post[0]->slug}}" />
                        <div class="form-group">
                            <label>Title</label>
                            <input class="form-control" type="text" name="title" value="{{$post[0]->title}}" placeholder="Tulis judul posting" required="required" />
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="SectionId">Section</label>
                                <select id="SectionId" class="form-control select" name="section_id">
                                    <option>Choose Section</option>
                                    @if(count($sections))
                                        @foreach($sections as $id => $section)
                                            @if($post[0]->section_id == $id)
                                                <option value="{{$id}}" selected>{{$section}}</option>
                                            @else
                                                <option value="{{$id}}">{{$section}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="CategoryId">Category</label>
                                <select id="CategoryId" class="form-control select" name="category_id">
                                    <option>Choose Category</option>
                                    @if(count($categories))
                                        @foreach($categories as $id => $category)
                                            @if($post[0]->category_id == $id)
                                                <option value="{{$id}}" selected>{{$category}}</option>
                                            @else
                                                <option value="{{$id}}">{{$category}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group admin-form">
                            <div class="section mbn">
                                <label class="field">
                                    <textarea id="textEditor" class="gui-textarea" name="konten" required="required">{{nl2br($post[0]->content)}}</textarea>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Cover</label>
                            <input type="file" class="form-control" name="cover" />
                        </div>
                        <div class="form-group mbn">
                            <button class="btn btn-primary" type="submit">Publish</button>
                            <button class="btn btn-warning" type="button">Save as Draft</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection