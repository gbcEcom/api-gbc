<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 11/17/2014
 * Time: 8:55 PM
 */

class Company extends Eloquent {
    //protected table
    protected $table = 'gbc_companies';

    public $timestamps = false;
} 