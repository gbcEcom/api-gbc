<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 11/17/2014
 * Time: 8:55 PM
 */

class Comment extends Eloquent {
    //protected table
    protected $table = 'comments';

    //Post
    public function Post(){
        return $this->hasMany('Post','post_id');
    }

    //Post
    public function Applicant(){
        return $this->belongsTo('Applicant','user_id','user_id');
    }
} 